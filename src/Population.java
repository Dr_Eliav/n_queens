import java.util.ArrayList;
import java.util.List;

public class Population {

  private static final int NUM_POPULATIONS = 10;
  private static final double MUTATION_PERCENTAGE = 0.2;

  private Chromosome[] chromosomes;
  private List<Chromosome> poolSelection;
  private int generation;
  private int bestFitnessIndex;

  public Population() {
    generation = 1;
    chromosomes = new Chromosome[NUM_POPULATIONS];

    for (int index = 0; index < NUM_POPULATIONS; index++) {
      chromosomes[index] = new Chromosome();
    }
  }

  public int bestFitness() {
    return chromosomes[bestFitnessIndex].fitness();
  }

  public int[] bestChromosome() {
    return chromosomes[bestFitnessIndex].gens();
  }

  public void initial() {
    for (int index = 0; index < NUM_POPULATIONS; index++) {
      chromosomes[index].setRandomValues();
    }
  }

  private void calcBestFitness() {
    bestFitnessIndex = 0;
    for (int index = 1; index < NUM_POPULATIONS; index++) {
      if (chromosomes[index].fitness() > chromosomes[bestFitnessIndex].fitness()) {
        bestFitnessIndex = index;
      }
    }
  }

  public void calcFitness() {
    for (int index = 0; index < NUM_POPULATIONS; index++) {
      chromosomes[index].calcFitness();
    }
    calcBestFitness();
  }

  public void selection() {
    poolSelection = new ArrayList<>();

    for (int index = 0; index < NUM_POPULATIONS; index++) {
      for (int count = 0; count < chromosomes[index].fitness(); count++) {
        poolSelection.add(chromosomes[index]);
      }
    }
  }

  public void crossover() {

    for (int index = 0; index < NUM_POPULATIONS; index++) {
      Chromosome partnerA = poolSelection.get(Main.random.nextInt(poolSelection.size()));
      Chromosome partnerB = poolSelection.get(Main.random.nextInt(poolSelection.size()));
      chromosomes[index] = new Chromosome(partnerA, partnerB);
    }
    generation++;
  }

  public void mutation() {
    for (int count = 0; count < MUTATION_PERCENTAGE * NUM_POPULATIONS; count++) {
      chromosomes[Main.random.nextInt(NUM_POPULATIONS)].mutation();
    }
  }

  public void printAllChromosomes() {
    System.out.println("--------------------------");
    System.out.println("Generation: " + generation);

    for (int index = 0; index < NUM_POPULATIONS; index++) {
      System.out.println(chromosomes[index].toString());
    }

    System.out.println(
        "Best fitness: " + (((double) bestFitness() / Chromosome.MAX_FITNESS) * 100) + "%");
  }
}
