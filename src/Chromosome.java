public class Chromosome {

  public static final int NUM_GENS = 8;
  public static final int MAX_FITNESS = NUM_GENS * 3;

  private int[] gens;
  private int fitness;

  public Chromosome() {
    gens = new int[NUM_GENS];
    fitness = 0;
  }

  public Chromosome(Chromosome partnerA, Chromosome partnerB) {
    this();
    int crossPoint = Main.random.nextInt(NUM_GENS);

    for (int index = 0; index < NUM_GENS; index++) {
      if (index < crossPoint) {
        gens[index] = partnerA.gens[index];
      } else {
        gens[index] = partnerB.gens[index];
      }
    }
  }

  public int[] gens() {
    return gens;
  }

  public int fitness() {
    return fitness;
  }

  public void setRandomValues() {
    for (int index = 0; index < NUM_GENS; index++) {
      gens[index] = Main.random.nextInt(NUM_GENS);
    }
  }

  private boolean isAloneInCol(int checkIndex) {
    boolean isAlone = true;

    for (int index = 0; index < NUM_GENS && isAlone; index++) {
      isAlone = (checkIndex == index || gens[checkIndex] != gens[index]);
    }

    return isAlone;
  }

  private boolean isAloneInFirstDiagonal(int checkIndex) {
    boolean isAlone = true;
    int[] temp = new int[NUM_GENS];

    for (int index = 0; index < NUM_GENS; index++) {
      temp[index] = gens[index] - Math.abs(checkIndex - index);
    }

    for (int index = 0; index < NUM_GENS && isAlone; index++) {
      isAlone = (checkIndex == index || temp[checkIndex] != temp[index]);
    }

    return isAlone;
  }

  private boolean isAloneInSecondDiagonal(int checkIndex) {
    boolean isAlone = true;
    int[] temp = new int[NUM_GENS];

    for (int index = 0; index < NUM_GENS; index++) {
      temp[index] = gens[index] + Math.abs(checkIndex - index);
    }

    for (int index = 0; index < NUM_GENS && isAlone; index++) {
      isAlone = (checkIndex == index || temp[checkIndex] != temp[index]);
    }

    return isAlone;
  }

  public void calcFitness() {
    for (int index = 0; index < NUM_GENS; index++) {
      if (isAloneInFirstDiagonal(index)) {
        fitness++;
      }
      if (isAloneInSecondDiagonal(index)) {
        fitness++;
      }
      if (isAloneInCol(index)) {
        fitness++;
      }
    }
  }

  public void mutation() {
    int genIndex = Main.random.nextInt(NUM_GENS);
    gens[genIndex] = Main.random.nextInt(NUM_GENS);
  }

  @Override
  public String toString() {
    StringBuilder strLocations = new StringBuilder("");

    for (int index = 0; index < NUM_GENS; index++) {
      strLocations.append((gens[index] + 1) + ", ");
    }

    strLocations.append("= " + fitness);

    return strLocations.toString();
  }
}
