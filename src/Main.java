import java.io.BufferedWriter;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.Random;

public class Main {

  public static final Random random = new Random();

  public static void printVisualBoard(int[] queens) {
    StringBuilder htmlPage =
        new StringBuilder("<!DOCTYPE html>\n"
            + "<html lang=\"en\">\n"
            + "\n"
            + "<head>\n"
            + "    <meta charset=\"UTF-8\">\n"
            + "    <meta http-equiv=\"X-UA-Compatible\" content=\"IE=edge\">\n"
            + "    <meta name=\"viewport\" content=\"width=device-width, initial-scale=1.0\">\n"
            + "    <link rel=\"stylesheet\" href=\"./style.css\">\n"
            + "    <title>N Queens</title>\n"
            + "</head>\n"
            + "\n"
            + "<body>\n"
            + "    <table class=\"chess-board\">"
            + "<tbody>\n"
            + "<tr> <th></th>");

    for (int index = 0; index < queens.length; index++) {
      htmlPage.append("<th>").append((char) (65 + index)).append("</th>");
    }
    htmlPage.append("</tr>");

    for (int row = 0; row < queens.length; row++) {
      htmlPage.append("<tr> <th>").append(queens.length - row).append("</th>");
      for (int col = 0; col < queens.length; col++) {
        if ((row + col) % 2 == 0) {
          htmlPage.append("<td class=\"light\">");
          if (queens[row] == col) {
            htmlPage.append("<img src=\"./images/queen1.png\" height=100 width=100>");
          }
        } else {
          htmlPage.append("<td class=\"dark\">");
          if (queens[row] == col) {
            htmlPage.append("<img src=\"./images/queen2.png\" height=100 width=100>");
          }
        }
        htmlPage.append("</td>");
      }
      htmlPage.append("</tr>");
    }
    htmlPage.append("</tbody>\n" + "</table>\n" + "</body>\n" + "\n" + "</html>");

    try (BufferedWriter bw = new BufferedWriter(new FileWriter("./VisualBoard/N_Queens.html"))) {
      bw.write(htmlPage.toString());
    } catch (Exception e) {
      System.out.println(e);
    }
  }

  public static void main(String[] args) {
    Population population = new Population();
    population.initial();
    population.calcFitness();
    population.printAllChromosomes();

    while (population.bestFitness() != Chromosome.MAX_FITNESS) {
      population.selection();
      population.crossover();
      population.mutation();
      population.calcFitness();
      population.printAllChromosomes();
    }

    printVisualBoard(population.bestChromosome());
  }
}
